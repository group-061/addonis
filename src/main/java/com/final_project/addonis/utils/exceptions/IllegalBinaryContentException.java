package com.final_project.addonis.utils.exceptions;

public class IllegalBinaryContentException extends RuntimeException {
    public IllegalBinaryContentException(String message) {
        super(message);
    }
}
